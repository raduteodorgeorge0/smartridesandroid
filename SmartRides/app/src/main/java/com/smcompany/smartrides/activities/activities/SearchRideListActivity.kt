package com.smcompany.smartrides.activities.activities

import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.beust.klaxon.Json
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.adapters.RideHistoryAdapter
import com.smcompany.smartrides.activities.adapters.RideSearchListAdapter
import com.smcompany.smartrides.activities.model.StandardRide


class SearchRideListActivity : AppCompatActivity() {
    companion object {
        lateinit var searchRideListRecyclerView: RecyclerView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_ride_list)
        val testAvailableRaids=intent.getStringExtra("availableRaids")





        val profileData=intent.getStringExtra("availableRaids")
        val jsonObject: JsonObject = JsonParser().parse(profileData).asJsonObject
        var jsonRideArray = jsonObject.get("rides").asJsonArray
        var rideSearchResultMutableList:MutableList<StandardRide> = arrayListOf()


        for (i in 0 until jsonRideArray.size()) {
            val iteratorRideJsonObject: JsonObject =
                JsonParser().parse(jsonRideArray[i].toString()).asJsonObject
            Log.d("RIDEOB",iteratorRideJsonObject.toString())

            val tmpStart = iteratorRideJsonObject.get("start").asString
            val tmpStop = iteratorRideJsonObject.get("finish").asString
            val tmpStartDistance = iteratorRideJsonObject.get("startDistance").asString
            val tmpEndDistance = iteratorRideJsonObject.get("endDistance").asString
            val tmpID = iteratorRideJsonObject.get("_id").asString

            var tmpStartLat = tmpStart.substringBefore(",").toDouble()
            var tmpStartLong = tmpStart.substringAfter(",").toDouble()
            var tmpStopLat = tmpStop.substringBefore(",").toDouble()
            var tmpStopLong = tmpStop.substringAfter(",").toDouble()

            //  Log.d("LAT","" + tmpStartLat + " @ " + tmpStartLong)

            val tmpStartAddress = getHumanAddress(tmpStartLat, tmpStartLong)
            val tmpStopAddress = getHumanAddress(tmpStopLat, tmpStopLong)

            val driverName = iteratorRideJsonObject.get("driver").asJsonObject.get("name").asString
            val carName = iteratorRideJsonObject.get("car").asJsonObject.get("name").asString
            val tmpStartDate = iteratorRideJsonObject.get("startDate").asString


            var tmpStandardRide = StandardRide(
                tmpID,
                    tmpStartLat,
                    tmpStartLong,
                    tmpStopLat,
                    tmpStopLong,
                driverName,
                carName,
                tmpStartDate.substring(0,10),
                tmpStartAddress,
                tmpStopAddress,
                tmpStartDistance.toString().substringBefore(".") + "." +  tmpStartDistance.toString().substringAfter(".").substring(0,1) + "KM" ,
                tmpEndDistance.toString().substringBefore(".") + "." + tmpEndDistance.toString().substringAfter(".").substring(0,1) + "KM" ,
                tmpStartDate.substring(11,16)

              //  driverName,
            //    carName,
             //   iteratorRideJsonObject.get("startDate").asString.substring(0, 10),
             //   tampStartAddress,
             //   tmpStopAddress,
             //   iteratorRideJsonObject.get("startDate").asString.substring(11, 16),
             //   iteratorRideJsonObject.get("finishDate")?.asString?.substring(11, 16)

            )

            rideSearchResultMutableList.add(tmpStandardRide)
        }

        searchRideListRecyclerView = findViewById<RecyclerView>(R.id.carListRecyclerView)
        searchRideListRecyclerView.layoutManager = LinearLayoutManager(this)
        searchRideListRecyclerView.adapter = RideSearchListAdapter(this,rideSearchResultMutableList.reversed().toMutableList())


    }
    private fun getHumanAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(this)
        val list = geocoder.getFromLocation(lat, lng, 1)
        return list[0].getAddressLine(0)
    }


}