package com.smcompany.smartrides.activities.model

import com.beust.klaxon.Json

class StandardRide (
    @Json(name = "_id")
    var _id:String,
    @Json(name = "startLat")
    var startLat: Double,
    @Json(name = "startLong")
    var startLong: Double,
    @Json(name = "finishLat")
    var finishLat: Double,
    @Json(name = "finishLong")
    var finishLong: Double,
    @Json(name = "driverName")
    var driverName: String,
    @Json(name = "car")
    var car: String,
    @Json(name = "startDate")
    var startDate: String,
    @Json(name = "start")
    var start: String,
    @Json(name = "finish")
    var finish: String,
    @Json(name = "startDistance")
    var startDistance: String,
    @Json(name = "stopDistance")
    var stopDistance: String,
    @Json(name = "startHour")
    var startHour: String

)