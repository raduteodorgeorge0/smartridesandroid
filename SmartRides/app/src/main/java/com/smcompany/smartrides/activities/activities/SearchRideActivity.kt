package com.smcompany.smartrides.activities.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import android.app.Dialog;
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.location.Address
import android.provider.Settings
import android.util.Log;
import android.widget.*
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.textfield.TextInputLayout
import com.schibstedspain.leku.*
import com.schibstedspain.leku.locale.SearchZoneRect
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import okhttp3.*
import okhttp3.RequestBody.Companion.toRequestBody
import org.w3c.dom.Text
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class TmpCoordsAndTime(
        @Json(name = "start")
        var start: String,
        @Json(name = "finish")
        var finish: String,
        @Json(name = "startDate")
        var startDate: String
)

class SearchRideActivity : AppCompatActivity() {

    private var startLat : String? = null
    private var startLong : String? = null
    private var stopLat : String? = null
    private var stopLong  : String? = null
    private var selectedTime : String? = null
    private var selectedDate : String? =  null
    var tokenValue = ""
    private val client = OkHttpClient()

    val loadingSpinner = LoadingSpinner(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_ride)

        try {
            val tokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
            tokenValue = tokenSharePref.getString("loginToken", "").toString()
        }catch (e : Exception)
        {}

        loadMapPickers()
        loadDatePickers()
        handlePostRide()


    }

    private fun handlePostRide()
    {


        var postSearchRideButton = findViewById<Button>(R.id.searchRidePostButton)

        postSearchRideButton.setOnClickListener {

            var selectedTimeStart = selectedTime
            var selectedDateStart = selectedDate


            Log.d("REQSX", "$startLat $startLong $selectedDateStart $selectedTimeStart")

            if(startLat == null || startLong == null || selectedDateStart== null || selectedTimeStart == null ) {
                Toast.makeText(this, "Locatie sau timp neselectat", Toast.LENGTH_SHORT).show()
            }else
            {
                loadingSpinner.startLoadingSpinner()
                var selectedDateStart = selectedDateStart + "T" + selectedTimeStart +":00.000Z"

                var tmoCoordsAndTime = TmpCoordsAndTime(
                        "$startLat,$startLong",
                        "$stopLat,$stopLong",
                        selectedDateStart
                )

                val searchRideJson = Klaxon().toJsonString(tmoCoordsAndTime)
                Log.d("ReqJSON",searchRideJson )
                val request = Request.Builder()
                        .url("https://apismartrides.xyz/api/ride/closest")
                        .header("auth",tokenValue)
                        .post( searchRideJson.toRequestBody(AddRideActivity.MEDIA_TYPE_JSON))
                     //   .header("start", startLat + "," + startLong)
                    //    .header("finish","$stopLat,$stopLong")
                    //    .header("startDate",selectedDateStart + "T" + selectedTimeStart +":00.000Z")
                    //    .header("finishDate",selectedDateStart + "T" + selectedTimeStart +":10.000Z")
                        .build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                        loadingSpinner.dismissLoadingSpinner()
                        Log.d("REQSX",e.toString())
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            if (!response.isSuccessful) {
                                loadingSpinner.dismissLoadingSpinner()
                                Log.d("REQSX",response.body!!.string())
                                throw IOException("Unexpected code $response")
                            } else {
                                loadingSpinner.dismissLoadingSpinner()
                                val intent = Intent(applicationContext, SearchRideListActivity::class.java)
                                intent.putExtra("availableRaids", response.body!!.string())
                                startActivity(intent)
                            }

                        }
                    }
                })
            }


        }
    }


    private fun loadMapPickers() {
        // OPEN GPS
       // var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
     //   startActivity(intent1);

        val startLocationLinearLayout  = findViewById<TextView>(R.id.startLocationTextView)
        startLocationLinearLayout.setOnClickListener {
                val locationPickerIntent = LocationPickerActivity.Builder()
                             .withLegacyLayout()
                   //// .withLocation(44.4268, 26.1025)
                    .build(applicationContext)

                startActivityForResult(locationPickerIntent, 1)
        }

        val stopLocationLinearLayout = findViewById<TextView>(R.id.stopLocationTextView)

        stopLocationLinearLayout.setOnClickListener {
            val locationPickerIntent = LocationPickerActivity.Builder()
                    .withLegacyLayout()

                   // .withLocation(44.4268, 26.1025)
                    .build(applicationContext)

            startActivityForResult(locationPickerIntent, 2)
        }

    }

    private fun loadDatePickers()
    {
        val timeInputField = findViewById<EditText>(R.id.FindRideTimeTextInputEditText)
        timeInputField.setOnClickListener {
            getTime(timeInputField,this)
        }

        val dateInputField = findViewById<EditText>(R.id.FindRideDateTextInputEditText)
        dateInputField.setOnClickListener {
            getDate(dateInputField,this)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT****", "OK")
            if (requestCode == 1) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())


               /* val fullAddress = data.getParcelableExtra<Address>(ADDRESS)
                if (fullAddress != null) {
                    Log.d("FULL ADDRESS****", fullAddress.toString())
                }*/


                val startLocationInputLayout = findViewById<TextView>(R.id.startLocationTextView)
                startLocationInputLayout.text = address
                startLat = latitude.toString()
                startLong = longitude.toString()


            } else if (requestCode == 2) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())


                val stopLocationTextView = findViewById<TextView>(R.id.stopLocationTextView)
                stopLocationTextView.text = address
                stopLat = latitude.toString()
                stopLong = longitude.toString()

            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT****", "CANCELLED")
        }
    }










    

    // -------- HANDLE DATE PICKERS ------------

    private fun getTime(textView: EditText, context: Context){
        val cal = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            textView.setText(SimpleDateFormat("HH:mm").format(cal.time).toString())
            selectedTime = SimpleDateFormat("HH:mm").format(cal.time).toString()
        }
        TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()

    }

    private fun getDate(textView: EditText, context: Context){
        val cal = Calendar.getInstance()
        val timeSetListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, month)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            textView.setText(SimpleDateFormat("yyyy-MM-dd").format(cal.time).toString())
            selectedDate = SimpleDateFormat("yyyy-MM-dd").format(cal.time).toString()
        }
        DatePickerDialog(this, timeSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show()

    }

}





