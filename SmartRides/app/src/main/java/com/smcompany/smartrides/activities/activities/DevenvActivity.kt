package com.smcompany.smartrides.activities.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import com.smcompany.smartrides.R
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException


class DevenvActivity : AppCompatActivity() {

     var testReqResponse : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_devenv)


        val skipAuthButton = findViewById<Button>(R.id.skipAuthButton)
        skipAuthButton.setOnClickListener {
            val intent = Intent(this,MainMapActivity::class.java)
            startActivity(intent)
        }


      //  val repository = Repository()
      //  val viewModelFactory = MainViewModelFactory(repository)

      //  viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)


        //var callSpinnerButton = findViewById<Button>(R.id.callSpinnerButton)

        //var loadingSpinner: LoadingSpinner = LoadingSpinner(this)

        val testapibutton = findViewById<Button>(R.id.getTestApiButton)
        testapibutton.setOnClickListener {
/*
            loadingSpinner.startLoadingSpinner()
            viewModel.getTestApi()
            viewModel.myTestResponse.observe(this, Observer { response ->
                if (response.isSuccessful) {
                    loadingSpinner.dismissLoadingSpinner()
                    Toast.makeText(this, response.body()!!.teststring, Toast.LENGTH_LONG).show()

                } else {
                    loadingSpinner.dismissLoadingSpinner()
                    Toast.makeText(this, response.code().toString(), Toast.LENGTH_LONG).show()
                }

            })
*/         /* fetchJson(this)
            if(testReqResponse != null)
            {
                Toast.makeText(this, testReqResponse, Toast.LENGTH_LONG).show()
            }else
            {
                Toast.makeText(this, "FAIIILED", Toast.LENGTH_LONG).show()
            }*/

            postRegister("https://apismartrides.xyz/api/user?key=CFFC-ESGC6", "", object : Callback {
                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        val responseStr = response.body!!.string()
                       Log.d("SUCX",responseStr)
                    } else {
                        Log.d("SUCX",response.toString())
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    Log.d("FLDXX",e.toString())
                }
            })
        }
    }


    private val client = OkHttpClient()

    fun fetchJson(context: Context) {
        println("Attempting to Fetch JSON")

        val url = "https://api.letsbuildthatapp.com/youtube/home_feed"

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {

            override fun onResponse(call: Call, response: Response) {
                val body = response?.body?.string()
                if (body != null) {
                    testReqResponse = body
                }
                val gson = GsonBuilder().create()
                Log.v("SUCX",testReqResponse.toString())

            }


            override fun onFailure(call: Call, e: IOException) {
                Log.v("FLD","Failed to execute request")
                testReqResponse = e.toString()
            }


        })
    }

    fun postRegisterTest() {
        val postBody = "{Gigel\n" +
                "    gigel@gmail.com,\n" +
                "    mata123123,\n" +
                "    0723882382,\n" +
                "}"

        val request = Request.Builder()
            .url("https://apismartrides.xyz/api/user?key=CFFC-ESGC6")
            .post(postBody.toRequestBody(MEDIA_TYPE_MARKDOWN))
            .build()

        client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")
            Log.d("SUCXX",response.body!!.string())
        }
    }




    fun postRegister(url: String?, json: String?, callback: Callback?): Call? {
        val postBody = json

        val request = Request.Builder()
                .url("https://apismartrides.xyz/api/user?key=CFFC-ESGC6")
                .post(postBody!!.toRequestBody(MEDIA_TYPE_MARKDOWN))
                .build()
        val call = client.newCall(request)
        call.enqueue(callback!!)
        return call
    }

    companion object {
        val MEDIA_TYPE_MARKDOWN = "application/json; charset=utf-8".toMediaType()
    }



}