package com.smcompany.smartrides.activities.activities

import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.beust.klaxon.Json
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.adapters.RideHistoryAdapter


class HistoryRide(
    @Json(name = "name")
    var name: String,
    @Json(name = "car")
    var car: String,
    @Json(name = "date")
    var date: String,
    @Json(name = "start")
    var start: String,
    @Json(name = "finish")
    var finish: String,
    @Json(name = "startDate")
    var startTime: String,
    @Json(name = "finishDate")
    var finishTime: String?


)

class RideHistoryListActivity : AppCompatActivity() {


    companion object {
        lateinit var rideHistoryRecyclerView: RecyclerView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride_history_list)

        val rideHistory = findViewById<TextView>(R.id.myRidesHistoryPlaceholder)




        val profileData=intent.getStringExtra("profileData")
        val jsonObject: JsonObject = JsonParser().parse(profileData).asJsonObject
        var jsonRideArray = jsonObject.get("rides").asJsonArray

        var rideHistoryMutableList:MutableList<HistoryRide> = arrayListOf()



        Log.d("LOCX",jsonRideArray.toString())
        for (i in 0 until jsonRideArray.size()) {
            val iteratorRideJsonObject: JsonObject = JsonParser().parse(jsonRideArray[i].toString()).asJsonObject
            val tmpStart = iteratorRideJsonObject.get("start").asString
            val tmpStop = iteratorRideJsonObject.get("finish").asString


            var tmpStartLat = tmpStart.substringBefore(",").toDouble()
            var tmpStartLong = tmpStart.substringAfter(",").toDouble()
            var tmpStopLat = tmpStop.substringBefore(",").toDouble()
            var tmpStopLong = tmpStop.substringAfter(",").toDouble()

          //  Log.d("LAT","" + tmpStartLat + " @ " + tmpStartLong)

            val tampStartAddress = getHumanAddress(tmpStartLat, tmpStartLong )
            val tmpStopAddress = getHumanAddress(tmpStopLat, tmpStopLong )

            val driverName = iteratorRideJsonObject.get("driver").asJsonObject.get("name").asString
            val carName = iteratorRideJsonObject.get("car").asJsonObject.get("name").asString


            var tmpRideObj = HistoryRide(
                driverName,
                carName,
                iteratorRideJsonObject.get("startDate").asString.substring(0,10),
                tampStartAddress,
                tmpStopAddress,
                iteratorRideJsonObject.get("startDate").asString.substring(11,16),
                iteratorRideJsonObject.get("finishDate")?.asString?.substring(11,16)

            )


            rideHistoryMutableList.add(tmpRideObj)
        }


        rideHistoryRecyclerView = findViewById<RecyclerView>(R.id.rideHistoryListRecyclerView)
        RideHistoryListActivity.rideHistoryRecyclerView.layoutManager = LinearLayoutManager(this)
        RideHistoryListActivity.rideHistoryRecyclerView.adapter = RideHistoryAdapter(this,rideHistoryMutableList.reversed().toMutableList())




    }

    private fun getHumanAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(this)
        val list = geocoder.getFromLocation(lat, lng, 1)
        return list[0].getAddressLine(0)
    }
}