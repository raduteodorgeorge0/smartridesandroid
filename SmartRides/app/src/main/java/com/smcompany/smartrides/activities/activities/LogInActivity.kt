package com.smcompany.smartrides.activities.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException


class loginUser(
        @Json(name = "email")
        var userEmail: String,
        @Json(name = "password")
        var userPassword: String
)


class LogInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)


        val signUpButton = findViewById<TextView>(R.id.signuptextviewsignin)

        signUpButton.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }


        var loadingSpinner = LoadingSpinner(this)

        /// Handle user input

        val signInUsernameField = findViewById<TextInputLayout>(R.id.usernameInputFieldLoginActivity)
        val signInPasswordField = findViewById<TextInputLayout>(R.id.passwordInputFieldLoginActivity)
        val loginButtonPost = findViewById<Button>(R.id.loginButtonPost)

        loginButtonPost.setOnClickListener {

            var tmpUser = loginUser(
                    signInUsernameField.editText?.text.toString(),
                    signInPasswordField.editText?.text.toString()
            )

            val loginJson = Klaxon().toJsonString(tmpUser)

            loadingSpinner.startLoadingSpinner()

            postLogin("https://apismartrides.xyz/api/user/login", loginJson ,this , object : Callback {
                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {

                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {

                                val jsonObject: JsonObject = JsonParser().parse(response.body?.string()).asJsonObject
                                val tmpToken = jsonObject.get("token").asString
                               // Toast.makeText(applicationContext, response.body?.string(), Toast.LENGTH_LONG).show()

                                val settings: SharedPreferences = applicationContext.getSharedPreferences("prefs", 0)
                                val editore = settings.edit()
                                editore.putString("loginToken", tmpToken );
                                editore.putString("profileData", jsonObject.toString())
                                editore.apply();

                                loadingSpinner.dismissLoadingSpinner()
                                val intent = Intent(applicationContext,MainMapActivity::class.java)
                                startActivity(intent)

                            }
                        } catch (e: Exception) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }
                            
                    } else {

                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {
                                loadingSpinner.dismissLoadingSpinner()
                                Toast.makeText(applicationContext, response.body?.string(), Toast.LENGTH_LONG).show()
                            }
                        } catch (e: Exception) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    val handler = Handler(Looper.getMainLooper())
                    try {
                        handler.post {
                            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })

        }



    }

    private val client = OkHttpClient()

    private fun postLogin(url: String, json: String, context1: Context, callback: Callback?): Call? {
        val postBody = json

        val request = Request.Builder()
                .url(url)
                .post(postBody.toRequestBody(MEDIA_TYPE_JSON))
                .build()
        val call = client.newCall(request)
        call.enqueue(callback!!)
        return call
    }

    companion object {
        val MEDIA_TYPE_JSON= "application/json; charset=utf-8".toMediaType();
    }
}