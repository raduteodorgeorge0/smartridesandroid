package com.smcompany.smartrides.activities.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.smcompany.smartrides.R


class ContactUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)

        var contactUsButtonEmail = findViewById<Button>(R.id.contactUsEmailButton)
        contactUsButtonEmail.setOnClickListener {
            val mailAdress = arrayOf("smartridesapp@gmail.com")
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_EMAIL, mailAdress)
            intent.putExtra(Intent.EXTRA_SUBJECT, "Support needed")
            intent.putExtra(Intent.EXTRA_TEXT, "Intrebarea mea este :")
            intent.type = "text/plain"
            startActivity(Intent.createChooser(intent, "Send Email using:"));
        }

    }
}