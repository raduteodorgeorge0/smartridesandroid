package com.smcompany.smartrides.activities.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.DrawableCompat
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.schibstedspain.leku.LATITUDE
import com.schibstedspain.leku.LOCATION_ADDRESS
import com.schibstedspain.leku.LONGITUDE
import com.schibstedspain.leku.LocationPickerActivity
import com.schibstedspain.leku.locale.SearchZoneRect
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import kotlinx.android.synthetic.main.activity_add_car.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.lang.Exception
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class TmpRide(
    @Json(name = "start")
    var start: String,
    @Json(name = "finish")
    var finish: String,
    @Json(name = "startDate")
    var startDate: String,
    @Json(name = "car")
    var car: String

)

class CarObjectWithId(
    @Json(name = "_id")
    var _id:String,
    @Json(name = "name")
    var name: String,
    @Json(name = "licencePlate")
    var licencePlate: String,
    @Json(name = "color")
    var color: String,
    @Json(name = "seats")
    var seats: Int,
    @Json(name = "consumption")
    var consumption: Int,
    @Json(name = "year")
    var year: Int

)

class AddRideActivity : AppCompatActivity() {

    private var startLat : String? = null
    private var startLong : String? = null
    private var stopLat : String? = null
    private var stopLong  : String? = null
    private var selectedTime : String? = null
    private var selectedDate : String? =  null
    private var carPosition: Int = 0
    var tokenValue = ""
    private val client = OkHttpClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_ride)

        var postCarButton = findViewById<Button>(R.id.addRidePostButton)
        var addCarsButton = findViewById<Button>(R.id.addNewCarInAddRideButton)


        /// ------- HANDLE CAR VIEWS ----
        val profileData=intent.getStringExtra("profileData")
        val jsonObject: JsonObject = JsonParser().parse(profileData).asJsonObject
        var carsList = getCarFromObjectAddCar(jsonObject)

        if (carsList.size == 0)
        {
            addCarsButton.setOnClickListener {
                var intent = Intent(this,AddCarActivity::class.java)
                startActivity(intent)
            }
        }
        else
        {
            var carSelectorSpinner = findViewById<Spinner>(R.id.addRideCarSpinner)
            postCarButton.visibility = View.VISIBLE
            carSelectorSpinner.visibility = View.VISIBLE
            addCarsButton.visibility = View.GONE
            var spinnerValues = mutableListOf<String>()
            for (item in carsList)
            {
                spinnerValues.add(item.licencePlate)
            }

            val spinnerArrayAdapter = ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerValues
            )

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            carSelectorSpinner.adapter = spinnerArrayAdapter


            carSelectorSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

                override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    //Toast.makeText(this@AddCarActivity,"You selected ${adapterView?.getItemAtPosition(position).toString()}",Toast.LENGTH_LONG).show()
                    //Toast.makeText(this@AddCarActivity,"You selected PL",Toast.LENGTH_LONG).show()
                    carPosition = position

                }


            }

        }

        /// -------- HANDLE SPINNER ------------

        try {
            val tokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
            tokenValue = tokenSharePref.getString("loginToken", "").toString()
        }catch (e : Exception)
        {}

        loadMapPickers()
        loadDatePickers()
        if(carsList.isNotEmpty())
        {
            handlePostRide(carsList[carPosition]._id)
        }



    }



    fun getCarFromObjectAddCar(carListJson : JsonObject) : List<CarObjectWithId>
    {
        var carJsonList =  carListJson.get("cars")
        val listType: Type = object : TypeToken<List<CarObjectWithId?>?>() {}.type
        return Gson().fromJson(carJsonList, listType)
    }




    private fun handlePostRide(carNumber : String)
    {
        var loadingSpinner = LoadingSpinner(this)
        var postaddRideButton = findViewById<Button>(R.id.addRidePostButton)

        postaddRideButton.setOnClickListener {
            var selectedTimeStart = selectedTime
            var selectedDateStart = selectedDate
            var finalTimeAndDateString = selectedDateStart + "T" + selectedTimeStart +":00.000Z"

            Log.d("REQSX", "$startLat $startLong $selectedDateStart $selectedTimeStart")

            var tmpLatLongStart = "$startLat,$stopLong"
            var tmpLatLongStop = "$stopLat,$stopLong"


            if(startLat == null || startLong == null || selectedDateStart== null || selectedTimeStart == null || stopLat == null || startLong == null) {
                Toast.makeText(this, "Locatie sau timp neselectat", Toast.LENGTH_SHORT).show()
            }else
            {
                loadingSpinner.startLoadingSpinner()
                var tmpCar = TmpRide(
                    tmpLatLongStart!!,
                    tmpLatLongStop!!,
                    finalTimeAndDateString,
                    carNumber
                )

                val addRideJson = Klaxon().toJsonString(tmpCar)
                Log.d("JSSS",addRideJson.toString())
                postRaid("https://apismartrides.xyz/api/ride", addRideJson ,this , object : Callback {
                    @Throws(IOException::class)
                    override fun onResponse(call: Call, response: Response) {
                        if (response.isSuccessful) {


                            val handler = Handler(Looper.getMainLooper())
                            try {
                                handler.post {
                                    loadingSpinner.dismissLoadingSpinner()
                                    Toast.makeText(applicationContext, "Successful Created Ride", Toast.LENGTH_LONG).show()
                                    finish()
                                  //  val intent = Intent(applicationContext,LogInActivity::class.java)
                                 //   startActivity(intent)

                                }
                            } catch (e: Exception) {
                                loadingSpinner.dismissLoadingSpinner()
                                e.printStackTrace()
                            }

                        } else {

                            val handler = Handler(Looper.getMainLooper())
                            try {
                                handler.post {
                                    loadingSpinner.dismissLoadingSpinner()
                                    Toast.makeText(applicationContext, response.body?.string(), Toast.LENGTH_LONG).show()
                                }
                            } catch (e: Exception) {
                                loadingSpinner.dismissLoadingSpinner()
                                e.printStackTrace()
                            }

                        }
                    }

                    override fun onFailure(call: Call, e: IOException) {
                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {
                                Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                })

            }


        }
    }


    private fun loadMapPickers() {
        // OPEN GPS
        // var intent1 = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        //   startActivity(intent1);

        val startLocationLinearLayout  = findViewById<TextView>(R.id.addRideStartLocationTextView)
        startLocationLinearLayout.setOnClickListener {
            val locationPickerIntent = LocationPickerActivity.Builder()
                .withLegacyLayout()
              //  .withSearchZone(SearchZoneRect(LatLng(44.4268, 26.1025), LatLng(44.9268, 27.0258746)))
                .build(applicationContext)

            startActivityForResult(locationPickerIntent, 1)
        }

        val stopLocationLinearLayout = findViewById<TextView>(R.id.addRideStopLocationTextView)

        stopLocationLinearLayout.setOnClickListener {
            val locationPickerIntent = LocationPickerActivity.Builder()
                .withLegacyLayout()
              //  .withSearchZone(SearchZoneRect(LatLng(44.4268, 26.1025), LatLng(44.9268, 27.0258746)))
              //  .withLocation(44.4268, 26.1025)
                .build(applicationContext)

            startActivityForResult(locationPickerIntent, 2)
        }



    }

    private fun loadDatePickers()
    {
        val timeInputField = findViewById<EditText>(R.id.addRideTimeTextInputEditText)
        timeInputField.setOnClickListener {
            getTime(timeInputField,this)
        }

        val dateInputField = findViewById<EditText>(R.id.addRideDateTextInputEditText)
        dateInputField.setOnClickListener {
            getDate(dateInputField,this)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT****", "OK")
            if (requestCode == 1) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())


                /* val fullAddress = data.getParcelableExtra<Address>(ADDRESS)
                 if (fullAddress != null) {
                     Log.d("FULL ADDRESS****", fullAddress.toString())
                 }*/


                val startLocationInputLayout = findViewById<TextView>(R.id.addRideStartLocationTextView)
                startLocationInputLayout.text = address
                startLat = latitude.toString()
                startLong = longitude.toString()


            } else if (requestCode == 2) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())


                val stopLocationTextView = findViewById<TextView>(R.id.addRideStopLocationTextView)
                stopLocationTextView.text = address
                stopLat = latitude.toString()
                stopLong = longitude.toString()

            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT****", "CANCELLED")
        }
    }





    // -------- HANDLE DATE PICKERS ------------

    private fun getTime(textView: EditText, context: Context){
        val cal = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            textView.setText(SimpleDateFormat("HH:mm").format(cal.time).toString())
            selectedTime = SimpleDateFormat("HH:mm").format(cal.time).toString()
        }
        TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()

    }

    private fun getDate(textView: EditText, context: Context){
        val cal = Calendar.getInstance()
        val timeSetListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, month)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            textView.setText(SimpleDateFormat("yyyy-MM-dd").format(cal.time).toString())
            selectedDate = SimpleDateFormat("yyyy-MM-dd").format(cal.time).toString()
        }
        DatePickerDialog(this, timeSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(
            Calendar.DAY_OF_MONTH)).show()

    }

    private fun postRaid(url: String, json: String, context1: Context, callback: Callback?): Call? {
        val postBody = json

        val request = Request.Builder()
            .url(url)
            .header("auth",tokenValue)
            .post(postBody.toRequestBody(MEDIA_TYPE_JSON))
            .build()
        val call = client.newCall(request)
        call.enqueue(callback!!)
        return call
    }

    companion object {
        val MEDIA_TYPE_JSON= "application/json; charset=utf-8".toMediaType();
    }
}



