package com.smcompany.smartrides.activities.activities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker
import com.smcompany.smartrides.R


/**
 * Created by User on 10/2/2017.
 */
class CustomInfoWindowAdapter(context: Context) : InfoWindowAdapter {
    private var mWindow: View
    private var mContext: Context

    init {
        mContext = context
        mWindow = LayoutInflater.from(context).inflate(R.layout.custom_info_window_maps, null)
    }


    private fun rendowWindowText(marker: Marker, view: View) {
        val title = marker.title
        val tvTitle = view.findViewById<View>(R.id.title) as TextView
        if (title != "") {
            tvTitle.text = title
        }
        val snippet = marker.snippet
        val tvSnippet = view.findViewById<View>(R.id.snippet) as TextView
        if (snippet != "") {
            tvSnippet.text = snippet
        }
    }

    override fun getInfoWindow(marker: Marker): View {
        rendowWindowText(marker, mWindow)
        return mWindow
    }

    override fun getInfoContents(marker: Marker): View {
        rendowWindowText(marker, mWindow)
        return mWindow
    }


}