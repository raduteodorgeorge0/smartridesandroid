package com.smcompany.smartrides.activities.activities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var loggedIn = false
        var tokenValue = ""

        try {
            val tokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
            tokenValue = tokenSharePref.getString("loginToken", "").toString()
        }catch (e : Exception)
        {

        }

        if(tokenValue != "")
            loggedIn = true

        if(!loggedIn)
        {
            val intent = Intent(this,LogInActivity::class.java)
            startActivity(intent)
        }
        else
        {
            val intent = Intent(this,MainMapActivity::class.java)
            startActivity(intent)
        }

    }
    
}







/* private lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var callApiButton = findViewById<Button>(R.id.apiGetButton)
        var apiDisplayText = findViewById<TextView>(R.id.apiText)

        val repository = Repository()
        val viewModelFactory = MainViewModelFactory(repository)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)


        var callSpinnerButton = findViewById<Button>(R.id.callSpinnerButton)

        var loadingSpinner: LoadingSpinner = LoadingSpinner(this)

        callApiButton.setOnClickListener {
            loadingSpinner.startLoadingSpinner()
            viewModel.getPost()
            viewModel.myResponse.observe(this, Observer { response ->
                if (response.isSuccessful) {
                    loadingSpinner.dismissLoadingSpinner()
                    Toast.makeText(this, response.body()?.body.toString(), Toast.LENGTH_LONG).show()

                } else {
                    loadingSpinner.dismissLoadingSpinner()
                    Toast.makeText(this, response.code().toString(), Toast.LENGTH_LONG).show()
                }

            })
        }



    } */