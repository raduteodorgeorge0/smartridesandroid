package com.smcompany.smartrides.activities.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.activities.CarObject
import kotlinx.android.synthetic.main.carlist_row.view.*


class CarListAdaptor(private var mContext: Context, private val arrList: MutableList<CarObject>) : RecyclerView.Adapter<CarItemHolder>()
{

    override fun getItemCount()= arrList.size
    var lastTappedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarItemHolder {
        val layoutInflater= LayoutInflater.from(parent?.context)
        val cellRow=layoutInflater.inflate(R.layout.carlist_row,parent,false)
        return CarItemHolder(cellRow,arrList)
    }
    override fun onBindViewHolder(holder: CarItemHolder, position: Int)
    {

        holder.bindItems(arrList[position],mContext)

    }



}

class CarItemHolder(view: View, arrList: List<CarObject>): RecyclerView.ViewHolder(view) {


    fun bindItems(itemModel: CarObject,mContext: Context) {
        try {

           // itemView.mItemTitle.text = itemModel.itemName.toString()
          //  itemView.mItemDescription.text =  itemModel.itemDescription.toString()
            var displayStringTest = itemModel.name +'\n' + "Numar:"+ itemModel.licencePlate
            itemView.carBrandTitleView.text = displayStringTest



            itemView.setOnClickListener {
                    /*
                    val position: Int = getAdapterPosition() // gets item position
                    if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                        // We can access the data within the views
                        itemView.background = ContextCompat.getDrawable(mContext, R.drawable.medium_outline)
                        Toast.makeText(mContext, position.toString(), Toast.LENGTH_SHORT).show()
                    }*/

            }

        } catch (e: Exception) {
            println(e.message)
        }
    }


}
