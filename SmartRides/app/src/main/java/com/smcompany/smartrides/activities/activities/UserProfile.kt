package com.smcompany.smartrides.activities.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.util.Strings
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.*
import okio.IOException
import org.w3c.dom.Text


class UserProfile : AppCompatActivity() {

    var jsonResponse : String = "Null"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        val profileData=intent.getStringExtra("profileData")
        val jsonObject: JsonObject = JsonParser().parse(profileData).asJsonObject




        val userName = findViewById<TextView>(R.id.nameProfileTextView)
        val profileEmail = findViewById<TextInputLayout>(R.id.profileEmailInputField)
        val userCompany = findViewById<TextInputLayout>(R.id.profileCompanyInputField)
        val profilePhoneNumber = findViewById<TextInputLayout>(R.id.profilePhoneNumberInputField)

        userName.text = jsonObject.get("name").asString
        profileEmail.hint = jsonObject.get("email").asString
        //userCompany.hint = jsonObject.get("company").asString
        profilePhoneNumber.hint = jsonObject.get("phoneNumber").asString




        val editCarButton = findViewById<Button>(R.id.editCarTextView)
        editCarButton.setOnClickListener{
            var intent = Intent(this,CarListActivity::class.java)
            intent.putExtra("profileData" ,profileData)
            startActivity(intent)
        }

        val saveButton = findViewById<TextView>(R.id.saveProfileButton)
        saveButton.setOnClickListener {

            if (profileData != null) {
                Toast.makeText(this,"TKN:" + profileData,Toast.LENGTH_LONG).show()
            }
        }

    }



}