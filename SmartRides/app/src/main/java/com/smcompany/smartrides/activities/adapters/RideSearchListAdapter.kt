package com.smcompany.smartrides.activities.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.activities.AddCarActivity
import com.smcompany.smartrides.activities.activities.SearchRideListActivity
import com.smcompany.smartrides.activities.activities.SearchRidePreviewActivity
import com.smcompany.smartrides.activities.model.StandardRide
import kotlinx.android.synthetic.main.activity_user_profile.view.*
import kotlinx.android.synthetic.main.findride_row.view.*


class RideSearchListAdapter(private var mContext: Context, private val arrList: MutableList<StandardRide>) : RecyclerView.Adapter<RideSearchItemHolder>()
{

    override fun getItemCount()= arrList.size
    var lastTappedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RideSearchItemHolder {
        val layoutInflater= LayoutInflater.from(parent?.context)
        val cellRow=layoutInflater.inflate(R.layout.findride_row,parent,false)
        return RideSearchItemHolder(cellRow,arrList)
    }
    override fun onBindViewHolder(holder: RideSearchItemHolder, position: Int)
    {

        holder.bindItems(arrList[position],mContext)

    }

}

class RideSearchItemHolder(view: View, arrList: List<StandardRide>): RecyclerView.ViewHolder(view) {


    fun bindItems(itemModel: StandardRide, mContext: Context) {
        try {

            // itemView.mItemTitle.text = itemModel.itemName.toString()
            //  itemView.mItemDescription.text =  itemModel.itemDescription.toString()
        //    var displayStringTest = itemModel.name +'\n' + "Numar:"+ itemModel.licencePlate
       //     itemView.carBrandTitleView.text = displayStringTest

            itemView.findRideRowNameTextView.text = itemModel.driverName
            itemView.findRideRowCarTextView.text = itemModel.car
            itemView.findRideDateTextView.text = itemModel.startDate
            itemView.findRideStartKM.text = itemModel.startDistance
            itemView.findRideStopKM.text = itemModel.stopDistance
            itemView.findRideRowStartTextView.text = itemModel.start
            itemView.findRideRowStopTextView.text = itemModel.finish
            itemView.findRideRowStartTimeTextView.text = itemModel.startHour
            var context = itemView.context;




            itemView.setOnClickListener {
                var intent = Intent(context, SearchRidePreviewActivity::class.java)
                intent.putExtra("rideData" , Gson().toJson(itemModel))
                context.startActivity(intent)

            }

        } catch (e: Exception) {
            println(e.message)
        }
    }


}