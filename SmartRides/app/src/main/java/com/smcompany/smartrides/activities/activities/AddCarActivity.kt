package com.smcompany.smartrides.activities.activities

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.DrawableCompat
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import com.google.android.gms.common.util.Strings
import com.google.android.material.textfield.TextInputLayout
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import kotlinx.android.synthetic.main.activity_add_car.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException


class CarObject(
        @Json(name = "name")
        var name: String,
        @Json(name = "licencePlate")
        var licencePlate: String,
        @Json(name = "color")
        var color: String,
        @Json(name = "seats")
        var seats: Int,
        @Json(name = "consumption")
        var consumption: Int,
        @Json(name = "year")
        var year: Int

)


class AddCarActivity : AppCompatActivity(),NumberPicker.OnValueChangeListener {

    private val client = OkHttpClient()
    private lateinit var selectedTextView : TextView
    private  var colorPosition : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_car)

        ///-- Handle car color change and spinner --

        var carColorImage = findViewById<ImageView>(R.id.addCarImageViewPreview)

        addCarColorSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //Toast.makeText(this@AddCarActivity,"You selected ${adapterView?.getItemAtPosition(position).toString()}",Toast.LENGTH_LONG).show()
                //Toast.makeText(this@AddCarActivity,"You selected PL",Toast.LENGTH_LONG).show()
                colorPosition = position
                var colorsArray = resources.getStringArray(R.array.colorIDs)
                DrawableCompat.setTint(
                        DrawableCompat.wrap(carColorImage.drawable),
                        Color.parseColor(colorsArray[position])

                );
            }


        }


        //--  Handle form data --


        var addCarAvailableSeatsTextView = findViewById<TextView>(R.id.addCarAvailableSeatsTextView)
        var addCarAvailableSeatsLinearLayout = findViewById<LinearLayout>(R.id.addCarAvailableSeatsLinearLayout)
        addCarAvailableSeatsLinearLayout.setOnClickListener {
            selectedTextView = addCarAvailableSeatsTextView
            showAvailableSeatsDialog()
        }


        var addCarConsumptionTextView = findViewById<TextView>(R.id.addCarConsumptionTextView)
        var addCarConsumptionLinearLayout = findViewById<LinearLayout>(R.id.addCarConsumptionLinearLayout)
        addCarConsumptionLinearLayout.setOnClickListener {
            selectedTextView = addCarConsumptionTextView
            showConsumptionDialog()
        }

        var addCarYearTextView = findViewById<TextView>(R.id.addCarYearTextView)
        var addCarYearLinearLayout = findViewById<LinearLayout>(R.id.addCarYearLinearLayout)
        addCarYearLinearLayout.setOnClickListener {
            selectedTextView = addCarYearTextView
            showYearDialog()
        }















        /// -- Handle Car post request --

        var loadingSpinner = LoadingSpinner(this)
        var postCarButton = findViewById<Button>(R.id.saveAddCarPostButton)

        val tokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
        val tokenValue: String = tokenSharePref.getString("loginToken", "").toString()


        postCarButton.setOnClickListener {

        loadingSpinner.startLoadingSpinner()

        var seats = addCarAvailableSeatsTextView.text.toString().toInt()
        var consumption = addCarConsumptionTextView.text.toString().toInt()
        var year = addCarYearTextView.text.toString().toInt()
        val addCarNameInputField = findViewById<TextInputLayout>(R.id.addCarNameInputField)
        val addCarIdentificationNumberInputLayout = findViewById<TextInputLayout>(R.id.addCarIdentificationNumberInputLayout)
        var colorsArray = resources.getStringArray(R.array.colorIDs)

        var tmpCar = CarObject(
                addCarNameInputField.editText?.text.toString(),
                addCarIdentificationNumberInputLayout.editText?.text.toString(),
                colorsArray[colorPosition].toString(),
                seats,
                consumption,
                year

        )

        val carJson = Klaxon().toJsonString(tmpCar)

        postCar("https://apismartrides.xyz/api/user/car", carJson ,tokenValue,this , object : Callback {
            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {

                    val handler = Handler(Looper.getMainLooper())
                    try {
                        handler.post {


                            loadingSpinner.dismissLoadingSpinner()
                            val intent = Intent(applicationContext,MainMapActivity::class.java)
                            startActivity(intent)

                        }
                    } catch (e: Exception) {
                        loadingSpinner.dismissLoadingSpinner()
                        e.printStackTrace()
                    }

                } else {

                    val handler = Handler(Looper.getMainLooper())
                    try {
                        handler.post {
                            loadingSpinner.dismissLoadingSpinner()
                            Toast.makeText(applicationContext, response.body?.string(), Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        loadingSpinner.dismissLoadingSpinner()
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call, e: IOException) {
                val handler = Handler(Looper.getMainLooper())
                try {
                    handler.post {
                        Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


        }




    }


    private fun postCar(url: String, json: String, tokenValue:String, context1: Context, callback: Callback?): Call? {

        val request = Request.Builder()
                .url(url)
                .post(json.toRequestBody(AddCarActivity.MEDIA_TYPE_JSON))
                .header("auth",tokenValue)
                .build()
        val call = client.newCall(request)
        call.enqueue(callback!!)
        return call
    }

    companion object {
        val MEDIA_TYPE_JSON= "application/json; charset=utf-8".toMediaType();
    }

    private fun showAvailableSeatsDialog() {
        val d = Dialog(this@AddCarActivity)
        d.setTitle("Available Seats")
        d.setContentView(R.layout.car_details_dialog)
        val b1 = d.findViewById(R.id.button1) as Button
        val np = d.findViewById(R.id.numberPicker1) as NumberPicker
        np.maxValue = 6
        np.minValue = 0
        np.wrapSelectorWheel = true
        np.setOnValueChangedListener(this)
        b1.setOnClickListener(object : DialogInterface.OnClickListener, View.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                selectedTextView.text = np.value.toString()
                d.dismiss()
            }

            override fun onClick(v: View?) {
                selectedTextView.text = np.value.toString()
                d.dismiss()
            }

        })
        d.show()
    }


    private fun showConsumptionDialog() {
        val d = Dialog(this@AddCarActivity)
        d.setTitle("Consumption")
        d.setContentView(R.layout.car_details_dialog)
        val b1 = d.findViewById(R.id.button1) as Button
        val np = d.findViewById(R.id.numberPicker1) as NumberPicker
        var consumptionValues = arrayOf("1","2","3","4","5")
        np.maxValue = consumptionValues.size - 1
        np.minValue = 0
        np.wrapSelectorWheel = true
        np.displayedValues = consumptionValues
        np.setOnValueChangedListener(this)
        b1.setOnClickListener(object : DialogInterface.OnClickListener, View.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                selectedTextView.text = consumptionValues[np.value]
                d.dismiss()
            }

            override fun onClick(v: View?) {
                selectedTextView.text = consumptionValues[np.value]
                d.dismiss()
            }

        })
        d.show()
    }

    private fun showYearDialog() {
        val d = Dialog(this@AddCarActivity)
        d.setTitle("Available Seats")
        d.setContentView(R.layout.car_details_dialog)
        val b1 = d.findViewById(R.id.button1) as Button
        val np = d.findViewById(R.id.numberPicker1) as NumberPicker
        np.maxValue = 20
        np.minValue = 0
        np.wrapSelectorWheel = true
        np.setOnValueChangedListener(this)
        b1.setOnClickListener(object : DialogInterface.OnClickListener, View.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                selectedTextView.text = np.value.toString()
                d.dismiss()
            }

            override fun onClick(v: View?) {
                selectedTextView.text = np.value.toString()
                d.dismiss()
            }

        })
        d.show()
    }




    override fun onValueChange(picker: NumberPicker?, oldVal: Int, newVal: Int) {
        Log.i("value is",""+newVal);
    }

}