package com.smcompany.smartrides.activities.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.adapters.CarListAdaptor
import java.lang.reflect.Type


class CarListActivity : AppCompatActivity() {

    companion object {

        lateinit var mRecyclerView: RecyclerView
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_list)

        val profileData=intent.getStringExtra("profileData")
        val jsonObject: JsonObject = JsonParser().parse(profileData).asJsonObject

        var carsList = getCarFromObject(jsonObject)

        var addCarButton = findViewById<Button>(R.id.carListAddCarButton)
        addCarButton.setOnClickListener {
            var intent = Intent(this,AddCarActivity::class.java)
            startActivity(intent)
        }
        mRecyclerView = findViewById(R.id.carListRecyclerView)
        mRecyclerView.layoutManager = LinearLayoutManager(this)

        val carListArray : MutableList<CarObject> = carsList.toMutableList()
        mRecyclerView.adapter = CarListAdaptor(this,carListArray)

    }

    fun getCarFromObject(carListJson : JsonObject) : List<CarObject>
    {
        var carJsonList =  carListJson.get("cars")
        val listType: Type = object : TypeToken<List<CarObject?>?>() {}.type

        val yourList: List<CarObject> = Gson().fromJson(carJsonList, listType)

        return yourList
    }


}