package com.smcompany.smartrides.activities.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.material.navigation.NavigationView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import kotlinx.android.synthetic.main.activity_main_map.*
import okhttp3.*
import okio.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainMapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private var map: GoogleMap? = null
    private var cameraPosition: CameraPosition? = null
    private val defaultLocation = LatLng(44.4268,26.1025)
    private var locationPermissionGranted = false
    lateinit var userStatusMapTextView: TextView
    var userStatusTextValue : String = " { \"standardStatus\" : \"Status : Nu exista curse setate\"} "
    private var lastKnownLocation: Location? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val client = OkHttpClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_map)
        userStatusMapTextView = findViewById<TextView>(R.id.userStatusMapTextView)
        val scheduleRideMapButton = findViewById<Button>(R.id.scheduleRideMapButton)
        val createDriverRideMapButton = findViewById<Button>(R.id.createDriverRideMapButton)

        val tokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
        val tokenValue: String = tokenSharePref.getString("loginToken", "").toString()

        val loadingSpinner = LoadingSpinner(this)

        scheduleRideMapButton.setOnClickListener {
            handleAddRide(tokenValue)
        }

        createDriverRideMapButton.setOnClickListener {
            val intent = Intent(this,SearchRideActivity::class.java)
            startActivity(intent)
        }

        
        // -- Initialize Map --
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)


        // -- Drawer --

        setSupportActionBar(findViewById(R.id.toolbar))

        navigation_view.setNavigationItemSelectedListener{
            when (it.itemId) {
                R.id.nav_profile -> {

                    loadingSpinner.startLoadingSpinner()
                    val request = Request.Builder()
                            .url("https://apismartrides.xyz/api/user/status")
                            .header("auth",tokenValue)
                            .build()

                    client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                        override fun onResponse(call: Call, response: Response) {
                            response.use {
                                if (!response.isSuccessful)
                                {
                                    loadingSpinner.dismissLoadingSpinner()
                                    throw IOException("Unexpected code $response")
                                }else
                                {
                                    loadingSpinner.dismissLoadingSpinner()
                                    val intent = Intent(applicationContext,UserProfile::class.java)
                                    intent.putExtra("profileData" ,response.body!!.string())
                                    startActivity(intent)
                                }

                            }
                        }
                    })


                }
                R.id.nav_myrides ->
                {
                    loadingSpinner.startLoadingSpinner()
                    val request = Request.Builder()
                            .url("https://apismartrides.xyz/api/user/status")
                            .header("auth",tokenValue)
                            .build()

                    client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                        override fun onResponse(call: Call, response: Response) {
                            response.use {
                                if (!response.isSuccessful)
                                {
                                    loadingSpinner.dismissLoadingSpinner()
                                    throw IOException("Unexpected code $response")
                                }else
                                {
                                    loadingSpinner.dismissLoadingSpinner()
                                    val intent = Intent(applicationContext,RideHistoryListActivity::class.java)
                                    intent.putExtra("profileData" ,response.body!!.string())
                                    startActivity(intent)
                                }

                            }
                        }
                    })
                }
                R.id.nav_notifications ->
                {
                    val intent = Intent(this,ContactUsActivity::class.java)
                    startActivity(intent)
                }
                R.id.nav_driverpanel ->
                {
                    loadingSpinner.startLoadingSpinner()
                    val request = Request.Builder()
                        .url("https://apismartrides.xyz/api/user/status/driver")
                        .header("auth",tokenValue)
                        .build()

                    client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                        override fun onResponse(call: Call, response: Response) {
                            response.use {
                                if (!response.isSuccessful)
                                {
                                    loadingSpinner.dismissLoadingSpinner()
                                    throw IOException("Unexpected code $response")
                                }else
                                {
                                    loadingSpinner.dismissLoadingSpinner()
                                    val intent = Intent(applicationContext,DriverPanelActivity::class.java)
                                    intent.putExtra("driverStatus" ,response.body!!.string())
                                    startActivity(intent)
                                }

                            }
                        }
                    })

                    val intent = Intent(this,DriverPanelActivity::class.java)
                    startActivity(intent)
                }
                R.id.nav_contact ->
                {
                    val intent = Intent(this,ContactUsActivity::class.java)
                    startActivity(intent)
                }
            }
            true
        }


        val profileData: String = tokenSharePref.getString("profileData", "").toString()
        val jsonObject: JsonObject = JsonParser().parse(profileData).asJsonObject
        val userObject: JsonObject = jsonObject.get("user").asJsonObject



        val drawerToggle = ActionBarDrawerToggle(this, drawer, R.string.open, R.string.close)
        drawer.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)





        val navigationView: NavigationView =
            findViewById<View>(R.id.navigation_view) as NavigationView
        val headerView: View = navigationView.getHeaderView(0)
        val navUsername = headerView.findViewById(R.id.userNameTextViewDrawer) as TextView
        var navPhoneNumber = headerView.findViewById(R.id.phoneNumberTextViewDrawer) as TextView


        navUsername.text = userObject.get("name").asString
        navPhoneNumber.text = userObject.get("phoneNumber").asString



        // updateStatusTextViewMapActivity(userStatusMapTextView,tokenValue)

        val timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                updateTimer()
            }
        }, 0, 5000)


    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun handleAddRide(tokenValue:String)
    {
        val request = Request.Builder()
            .url("https://apismartrides.xyz/api/user/status")
            .header("auth",tokenValue)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful)
                    {
                        throw IOException("Unexpected code $response")
                    }else
                    {
                        val intent = Intent(applicationContext,AddRideActivity::class.java)
                        intent.putExtra("profileData" ,response.body!!.string())
                        startActivity(intent)
                    }

                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        return when (item.itemId) {
            android.R.id.home -> {
                drawer.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap?) {



        getLocationPermission()


        // Turn on the My Location layer and the related control on the map.
        updateLocationUI()

        // Get the current location of the device and set the position of the map.
        getDeviceLocation()

        this.map = map
        val zoomLevel = 10.0f //This goes up to 21
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, zoomLevel))

        if(locationPermissionGranted)
        {
              map?.isMyLocationEnabled = true
              map?.uiSettings!!.isZoomControlsEnabled = false
              map?.setOnMarkerClickListener(this)}

        }



    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(lastKnownLocation!!.latitude,
                                            lastKnownLocation!!.longitude), DEFAULT_ZOOM.toFloat()))
                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        map?.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(defaultLocation, DEFAULT_ZOOM.toFloat()))
                        map?.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }
    // [END maps_current_place_get_device_location]

    /**
     * Prompts the user for permission to use the device location.
     */
    // [START maps_current_place_location_permission]
    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }
    // [END maps_current_place_location_permission]

    /**
     * Handles the result of the request for location permissions.
     */
    // [START maps_current_place_on_request_permissions_result]
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }
    // [END maps_current_place_on_request_permissions_result]

    /**
     * Prompts the user to select the current place from a list of likely places, and shows the
     * current place on the map - provided the user has granted location permission.
     */
    // [START maps_current_place_show_current_place]

    private fun updateLocationUI() {
        if (map == null) {
            return
        }
        try {
            if (locationPermissionGranted) {
                map?.isMyLocationEnabled = true
                map?.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }
    // [END maps_current_place_update_location_ui]

    companion object {
        private val TAG = MainMapActivity::class.java.simpleName
        private const val DEFAULT_ZOOM = 15
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1

        // Keys for storing activity state.
        // [START maps_current_place_state_keys]
        private const val KEY_CAMERA_POSITION = "camera_position"
        private const val KEY_LOCATION = "location"
        // [END maps_current_place_state_keys]

        // Used for selecting the current place.
        private const val M_MAX_ENTRIES = 5
    }

    override fun onMarkerClick(p0: Marker?) = false


     private fun updateStatusTextViewMapActivity(mTextView: TextView,tokenValue:String)
    {


    }




    private fun updateTimer() {

        val tmpTokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
        val tmpTokenValue: String = tmpTokenSharePref.getString("loginToken", "").toString()

        runOnUiThread {

            val request = Request.Builder()
                .url("https://apismartrides.xyz/api/user/status/ride")
                .header("auth",tmpTokenValue)
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful)
                        {
                            throw IOException("Unexpected code $response")
                        }else
                        {
                            userStatusTextValue = response.body!!.string()
                        }

                    }
                }
            })

            val jsonObject: JsonObject = JsonParser().parse(userStatusTextValue).asJsonObject

            if (jsonObject.has("startDate"))
            {
                userStatusMapTextView.text = "Status: Urmeaza cursa la ora " + jsonObject.get("startDate").asString.substring(11,16)
            }
            else
            {
                userStatusMapTextView.text = "Status : Nu exista curse setate"
            }



        }



    }





}



