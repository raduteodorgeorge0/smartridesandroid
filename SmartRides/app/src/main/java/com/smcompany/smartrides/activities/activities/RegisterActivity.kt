package com.smcompany.smartrides.activities.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import com.google.android.material.textfield.TextInputLayout
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException


class RegisterUser(
        @Json(name = "name")
        var registerName: String,
        @Json(name = "email")
        var registerEmail: String,
        @Json(name = "password")
        var registerPassword: String,
        @Json(name = "phoneNumber")
        var userPhone: String
)

class RegisterActivity : AppCompatActivity() {

    var registerContext : Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


        val registerNameInputLayout = findViewById<TextInputLayout>(R.id.registerNameInputField)
        val registerEmailInputLayout = findViewById<TextInputLayout>(R.id.registerEmailInputField)
        val registerPasswordInputLayout = findViewById<TextInputLayout>(R.id.registerPasswordInputField)
        val registerPhoneNumberInputLayout = findViewById<TextInputLayout>(R.id.registerPhoneNumberInputField)
        val registerCompanyCodeInputField = findViewById<TextInputLayout>(R.id.registerCompanyCodeInputField)

        val registerSubmitPostButton = findViewById<Button>(R.id.registerSubmitButton)

        var loadingSpinner = LoadingSpinner(this)


        registerSubmitPostButton.setOnClickListener {
            var tmpUser = RegisterUser(
                    registerNameInputLayout.editText?.text.toString(),
                    registerEmailInputLayout.editText?.text.toString(),
                    registerPasswordInputLayout.editText?.text.toString(),
                    registerPhoneNumberInputLayout.editText?.text.toString()
            )

            val registerJson = Klaxon().toJsonString(tmpUser)

            loadingSpinner.startLoadingSpinner()

            postRegister("https://apismartrides.xyz/api/user?key=" + registerCompanyCodeInputField.editText?.text.toString(), registerJson ,this , object : Callback {
                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {


                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {
                                loadingSpinner.dismissLoadingSpinner()
                                Toast.makeText(applicationContext, "Successful Register. You can login now", Toast.LENGTH_LONG).show()

                                val intent = Intent(applicationContext,LogInActivity::class.java)
                                startActivity(intent)

                            }
                        } catch (e: Exception) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                    } else {

                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {
                                loadingSpinner.dismissLoadingSpinner()
                                Toast.makeText(applicationContext, response.body?.string(), Toast.LENGTH_LONG).show()
                            }
                        } catch (e: Exception) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    val handler = Handler(Looper.getMainLooper())
                    try {
                        handler.post {
                            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })


        }

    }

    private val client = OkHttpClient()

    private fun postRegister(url: String, json: String, context1: Context, callback: Callback?): Call? {
        val postBody = json

        val request = Request.Builder()
                .url(url)
                .post(postBody.toRequestBody(MEDIA_TYPE_JSON))
                .build()
        val call = client.newCall(request)
        call.enqueue(callback!!)
        return call
    }

    companion object {
        val MEDIA_TYPE_JSON= "application/json; charset=utf-8".toMediaType();
    }

}