package com.smcompany.smartrides.activities.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.loaders.LoadingSpinner
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException

class joinLocation (
    @Json(name = "startLocation")
    var startLocation: String,
    @Json(name = "finish")
    var stopLocation: String
)

class SearchRidePreviewActivity : AppCompatActivity() {

    private val client = OkHttpClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_ride_preview)


        val previewRouteButton = findViewById<Button>(R.id.previewRouteOnMapButton)
        val joinRideButton = findViewById<Button>(R.id.previewJoinRideButton)

        val rideData=intent.getStringExtra("rideData")
        val rideObject: JsonObject = JsonParser().parse(rideData).asJsonObject

        val startLat = rideObject.get("startLat").asDouble
        val startLong = rideObject.get("startLong").asDouble
        val finishLat = rideObject.get("finishLat").asDouble
        val finishLong = rideObject.get("finishLong").asDouble



        val rideID = rideObject.get("_id").asString
        val driverName = rideObject.get("driverName").asString
        val carName = rideObject.get("car").asString
        val startHour = rideObject.get("startHour").asString
        val start = rideObject.get("start").asString
        val stop = rideObject.get("finish").asString



        val namePreviewTextView = findViewById<TextView>(R.id.previewRideDriverNameTextView)
        namePreviewTextView.text = driverName


        val carNamePreviewTextView = findViewById<TextView>(R.id.previewCarNameTextView)
        carNamePreviewTextView.text = carName


        val previewStartLocationTextView = findViewById<TextView>(R.id.previewStartLocationTextView)
        previewStartLocationTextView.text = start

        val previewStopLocationTextView = findViewById<TextView>(R.id.previewStopLocationTextView)
        previewStopLocationTextView.text = stop

        val previewStartTimeTextView = findViewById<TextView>(R.id.previewTimeLocationTextView)
        previewStartTimeTextView.text = startHour



        val tokenSharePref: SharedPreferences = getSharedPreferences("prefs", 0)
        val tokenValue: String = tokenSharePref.getString("loginToken", "").toString()
        var loadingSpinner = LoadingSpinner(this)



        val tmpJoinLocation = joinLocation(
            "$startLat,$startLong",
            "$finishLat,$finishLong"
        )

        val joinLocationJson = Klaxon().toJsonString(tmpJoinLocation)


        previewRouteButton.setOnClickListener {
            launchGoogleMapsWithRoute(startLat, startLong,finishLat, finishLong)
        }

        joinRideButton.setOnClickListener{
            /// Cal api

            loadingSpinner.startLoadingSpinner()
            joinRide("https://apismartrides.xyz/api/ride/join/$rideID",joinLocationJson,tokenValue,this , object : Callback {
                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {

                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {


                                loadingSpinner.dismissLoadingSpinner()
                                Toast.makeText(applicationContext, "Ride Added", Toast.LENGTH_LONG).show()
                                val intent = Intent(applicationContext,MainMapActivity::class.java)
                                startActivity(intent)

                            }
                        } catch (e: Exception) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                    } else {

                        val handler = Handler(Looper.getMainLooper())
                        try {
                            handler.post {
                                loadingSpinner.dismissLoadingSpinner()
                                Toast.makeText(applicationContext, response.body?.string(), Toast.LENGTH_LONG).show()
                            }
                        } catch (e: Exception) {
                            loadingSpinner.dismissLoadingSpinner()
                            e.printStackTrace()
                        }

                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    val handler = Handler(Looper.getMainLooper())
                    try {
                        handler.post {
                            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })


        }

    }

    fun launchGoogleMapsWithRoute(startLat:Double, startLong:Double, stopLat:Double, stopLong:Double){

        var mUriString = "https://www.google.com/maps/dir/?api=1&origin=$startLat,$startLong&destination=$stopLat,$stopLong&travelmode=driving"

        val intent = Intent(Intent.ACTION_VIEW,
                Uri.parse(mUriString))
        startActivity(intent)
    }

    private fun getHumanAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(this)
        val list = geocoder.getFromLocation(lat, lng, 1)
        return list[0].getAddressLine(0)
    }


    private fun joinRide(url: String,joinRideBody:String, tokenValue:String, context1: Context, callback: Callback?): Call? {
        /// Add the following JSON as as body startLocation: lat,long | stopLocation:lat,long
        val request = Request.Builder()
                .url(url)
                .post(joinRideBody.toRequestBody(AddCarActivity.MEDIA_TYPE_JSON))
                .header("auth",tokenValue)
                .build()
        val call = client.newCall(request)
        call.enqueue(callback!!)
        return call
    }

    companion object {
        val MEDIA_TYPE_JSON= "application/json; charset=utf-8".toMediaType();
    }
}