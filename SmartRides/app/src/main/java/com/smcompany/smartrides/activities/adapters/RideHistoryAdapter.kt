package com.smcompany.smartrides.activities.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.smcompany.smartrides.R
import com.smcompany.smartrides.activities.activities.CarObject
import com.smcompany.smartrides.activities.activities.HistoryRide
import kotlinx.android.synthetic.main.carlist_row.view.*
import kotlinx.android.synthetic.main.ridehistory_row.view.*


class RideHistoryAdapter(private var mContext: Context, private val arrList: MutableList<HistoryRide>) : RecyclerView.Adapter<RideItemHolder>()
{

    override fun getItemCount()= arrList.size
    var lastTappedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RideItemHolder {
        val layoutInflater= LayoutInflater.from(parent?.context)
        val cellRow=layoutInflater.inflate(R.layout.ridehistory_row,parent,false)
        return RideItemHolder(cellRow,arrList)
    }
    override fun onBindViewHolder(holder: RideItemHolder, position: Int)
    {

        holder.bindItems(arrList[position],mContext)

    }

}

class RideItemHolder(view: View, arrList: List<HistoryRide>): RecyclerView.ViewHolder(view) {


    fun bindItems(itemModel: HistoryRide,mContext: Context) {
        try {

            // itemView.mItemTitle.text = itemModel.itemName.toString()
            //  itemView.mItemDescription.text =  itemModel.itemDescription.toString()
            var displayStringTest = "1"

            var displayDriverName = itemModel.name
            var displayCarName = itemModel.car

            var rideHistoryRowStartTimeValue = itemModel.startTime
            var rideHistoryRowStopTimeValue = itemModel.finishTime

            var rideHistoryStartLocationValue = itemModel.start
            var rideHistoryStopLocationValue = itemModel.finish

            var rideHistoryStartCalendarDateValue = itemModel.date



            itemView.rideHistoryRowNameTextView.text = displayDriverName
            itemView.rideHistoryRowCarTextView.text = displayCarName
            itemView.rideHistoryDateTextView.text = rideHistoryStartCalendarDateValue

            itemView.rideHistoryRowStartTextView.text = rideHistoryStartLocationValue
            itemView.rideHistoryRowStartTimeTextView.text = rideHistoryRowStartTimeValue

            itemView.rideHistoryRowStopTextView.text = rideHistoryStopLocationValue
            itemView.rideHistoryRowStopTimeTextView.text = rideHistoryRowStopTimeValue


        } catch (e: Exception) {
            println(e.message)
        }
    }


}
